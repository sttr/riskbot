// Load required modules and config
const { SlashCommandBuilder, ChannelType, PermissionsBitField } = require('discord.js');
const { mysql_host, mysql_username, mysql_password, mysql_database } = require('../riskbot_config.json');
const { updateallowedChannelIds, updatechatChannelIds } = require('../modules/signuphandler.js');

var mysql = require('mysql2');

// Register command
module.exports = {
	data: new SlashCommandBuilder()
	.setName('create-event')
	.setDescription('Create an event (the bot will create all roles, channels and threads for you')
	.addStringOption(option =>
		option.setName('eventname')
		.setDescription('Event name')
		.setRequired(true))
	,
async execute(interaction, client) {
	try {

		await interaction.reply({ content: "Please stand by while I create the event for you...", ephemeral: true });

		const interactionUser = await interaction.guild.members.fetch(interaction.user.id);
		const eventname = interaction.options.getString('eventname').replace(/[^a-zA-Z0-9 ]/g, '').substring(0, 50);

		const guild = await client.guilds.fetch(interaction.guild.id);
		if (!guild) {
			console.log('Guild not found');
			return;
		}
		const botMember = guild.members.me; // Use members.me instead of guild.me
		if (!botMember) {
			console.log('Bot is not a member of the guild');
			return;
		}
		
		if (!botMember.permissions.has('ManageRoles')) {
			await interaction.followUp({ content: `Sorry, I don't have the correct permissions in this server to manage roles`, ephemeral: true });
			return;
		}

		// Connect to SQL database
		var con = mysql.createConnection({
			host: mysql_host,
			user: mysql_username,
			password: mysql_password,
			supportBigNumbers: true,
			bigNumberStrings: true
		});
		
		con.connect(function(err) {
			if (err) throw err;
			console.log("Connected to MySQL server!");
		});

		var errors = "";


		try {

			let sql = "INSERT INTO `"+ mysql_database +"`.`eventmanager__events` VALUES (NULL,'NORMAL','"+ eventname +"',"+ interaction.user.id +",NOW(),NULL,NULL,"+ guild.id +",1,'CLOSED',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0,0,0,1,0,0,0)";
			let result = await new Promise((resolve, reject) => {
				con.query(sql, function (err, result) {
					if (err) reject(err);
					resolve(result);
				});
			});
			const eventid = result.insertId;

			let sql_round = "INSERT INTO `"+ mysql_database +"`.`eventmanager__rounds` VALUES (NULL,"+ eventid +",1,1,'Round 1','DRAFT',6,4,'POINTS','WAITLIST',0,1,NULL,NULL,NULL,NULL,NULL,'This is your groups thread for this round, where you can confirm that everyone is ready, share lobby code before joinging the game and posting the results afterwards.\n## Your game is scheduled ##COUNTDOWN## at ##GAMETIME## (your local timezone)\nThe settings you are playing are shown in the attached images. If you have any question please click the buttons below or ask your fellow participants.\n## When the game is finished\nJust write the results in this thread and event staff will pick it up')";
			let result_round = await new Promise((resolve, reject) => {
				con.query(sql_round, function (err, result) {
					if (err) reject(err);
					resolve(result);
				});
			});

			sql = "INSERT INTO `"+ mysql_database +"`.`eventmanager__events_status` VALUES (NULL,"+ eventid +",NOW(),NULL,'OPEN')";
			result_status = await new Promise((resolve, reject) => {
				con.query(sql, function (err, result) {
					if (err) reject(err);
					resolve(result);
				});
			});

			const adminRoleName = "E"+eventid+"-host";
			const participantRoleName = "E"+eventid+"-participant";
			const waitlistRoleName = "E"+eventid+"-waitlist";
			const staffRoleName = "E"+eventid+"-staff";
			const noshowRoleName = "E"+eventid+"-noshow-bracket1";

			const noshowRole = await guild.roles.create({
				name: noshowRoleName, // Replace with your desired role name
				reason: 'Role created for those noshowing',
				mentionable: false // Ensure role is not pingable

			});

			const staffRole = await guild.roles.create({
				name: staffRoleName, // Replace with your desired role name
				reason: 'Role created to be event staff',
				mentionable: false // Ensure role is not pingable

			});

			const adminRole = await guild.roles.create({
				name: adminRoleName, // Replace with your desired role name
				reason: 'Role created to be event host',
				mentionable: false // Ensure role is not pingable

			});

			const participantRole = await guild.roles.create({
				name: participantRoleName, // Replace with your desired role name
				reason: 'Role created to be event participant',
				mentionable: false // Ensure role is not pingable
			});

			const waitlistRole = await guild.roles.create({
				name: waitlistRoleName, // Replace with your desired role name
				reason: 'Role created for waitlist players in event',
				mentionable: false // Ensure role is not pingable
			});

			let sql_bracket = "INSERT INTO `"+ mysql_database +"`.`eventmanager__brackets` VALUES (NULL,"+ eventid +",1,'"+ noshowRole.id +"','Bracket 1')";
			let result_bracket = await new Promise((resolve, reject) => {
				con.query(sql_bracket, function (err, result) {
					if (err) reject(err);
					resolve(result);
				});
			});


			await interactionUser.roles.add(staffRole);
			await interactionUser.roles.add(adminRole);


			const channel = await guild.channels.create({
				name: eventname,
				icon: "📢",
				type: ChannelType.GuildText, // You can change this to GuildVoice if you want a voice channel
				permissionOverwrites: [
					{
						id: guild.id,
						allow: [
							PermissionsBitField.Flags.ViewChannel,
							PermissionsBitField.Flags.ReadMessageHistory,
							PermissionsBitField.Flags.AttachFiles
						],
						deny: [
							PermissionsBitField.Flags.SendMessages // Prevent @everyone from sending messages
						]
					},
					{
						id: botMember.roles.botRole.id, // Set full permissions for the bot's role
						allow: [
							PermissionsBitField.Flags.ViewChannel,
							PermissionsBitField.Flags.SendMessages,
							PermissionsBitField.Flags.ManageChannels,
							PermissionsBitField.Flags.ManageMessages,
							PermissionsBitField.Flags.ReadMessageHistory,
							PermissionsBitField.Flags.ManageThreads,
							PermissionsBitField.Flags.MentionEveryone,
							PermissionsBitField.Flags.AttachFiles
						],
					},
					{
						id: adminRole.id, // Set the permissions for the newly created role
						allow: [
							PermissionsBitField.Flags.ViewChannel,
							PermissionsBitField.Flags.SendMessages,
							PermissionsBitField.Flags.ManageChannels,
							PermissionsBitField.Flags.ManageMessages,
							PermissionsBitField.Flags.EmbedLinks,
							PermissionsBitField.Flags.ReadMessageHistory,
							PermissionsBitField.Flags.ManageThreads,
							PermissionsBitField.Flags.AttachFiles
						],
					},
					{
						id: staffRole.id, // Set the permissions for the newly created role
						allow: [
							PermissionsBitField.Flags.ViewChannel,
							PermissionsBitField.Flags.ManageChannels,
							PermissionsBitField.Flags.ManageMessages,
							PermissionsBitField.Flags.EmbedLinks,
							PermissionsBitField.Flags.ReadMessageHistory,
							PermissionsBitField.Flags.ManageThreads,
							PermissionsBitField.Flags.AttachFiles
						],
					},
			// Add more permission overwrites if needed
				],
				defaultAutoArchiveDuration: 10080,
				reason: 'Event channel', // Optional
			});













			const signupchannel = await channel.threads.create({
				name: "📝 signup",
				icon: "📝",
				type: ChannelType.PublicThread,
				autoArchiveDuration: 10080,
			});

			const textchannel = await channel.threads.create({
				name: "🗣️ chat",
				icon: "🗣️",
				type: ChannelType.PublicThread,
				autoArchiveDuration: 10080,
			});

			const helpchannel = await channel.threads.create({
				name: "👋 help",
				icon: "👋",
				type: ChannelType.PublicThread,
				autoArchiveDuration: 10080,
			});

			const staffchannel = await channel.threads.create({
				name: "🛂 staff",
				icon: "🛂",
				type: ChannelType.PrivateThread,
				autoArchiveDuration: 10080,
			});

			staffchannel.members.add(botMember.id);
			staffchannel.members.add(interactionUser.id);	

			await staffchannel.send(`This will be your staff thread, where only event staff will have access`);
			await signupchannel.send(`### Signups are currently closed. Please check back later.`);


			sql = "UPDATE `"+ mysql_database +"`.`eventmanager__events` SET `mainchannel` = "+ channel.id +", `signupchannel` = "+ signupchannel.id +",`helpchannel` = "+ helpchannel.id +",`textchannel` = "+ textchannel.id +",`staffchannel` = "+ staffchannel.id +",`participantrole` = "+ participantRole.id +",`staffrole` = "+ staffRole.id +",`waitlistrole` = "+ waitlistRole.id +" WHERE `id` = "+ eventid +"";
			result = await new Promise((resolve, reject) => {
				con.query(sql, function (err, result) {
					if (err) reject(err);
					resolve(result);
				});
			});

			let welcomemsg = await channel.send(`Welcome to ${eventname}\n\nYou will find any announcements regarding this event in here.\n\nEvent webpage (rules, signups, groups, standings, rounds, settings will be published here): https://friendsofrisk.com/eventmanager/${eventid}\n\nSignup: https://discord.com/channels/${guild.id}/${signupchannel.id}\nChat: https://discord.com/channels/${guild.id}/${textchannel.id}\nHelp thread: https://discord.com/channels/${guild.id}/${helpchannel.id}\n\nGood luck!`);
			await welcomemsg.pin();

			errors = `Your event is created and ready for you to configure and open signups at https://friendsofrisk.com/eventmanager/${eventid}`;

			await updateallowedChannelIds();
			await updatechatChannelIds();

			sql = "INSERT INTO `"+ mysql_database +"`.`eventmanager__admins` VALUES ("+ eventid +","+ interaction.user.id +")";
			result = await new Promise((resolve, reject) => {
				con.query(sql, function (err, result) {
					if (err) reject(err);
					resolve(result);
				});
			});

		} catch (error) {
			console.error(error);
			if (interaction.replied || interaction.deferred) {
				await interaction.followUp({ content: "Error, please try again later", ephemeral: true });
			} else {
				await interaction.reply({ content: "Error, please try again later", ephemeral: true });
			}
		}

		con.end();

		await interaction.followUp({ content: errors, ephemeral: true });

	} catch (error) {
		console.error(error);
		await interaction.reply({ content: "Error, please try again later", ephemeral: true });
		}	
	}
};
